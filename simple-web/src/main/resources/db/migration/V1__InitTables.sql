CREATE TABLE item
(
  id SERIAL NOT NULL CONSTRAINT pk_item PRIMARY KEY,
  code VARCHAR(4) NOT NULL,
  model VARCHAR(40),
  ordinal INTEGER NOT NULL CHECK (ordinal > 0),
  size VARCHAR(2)
);

CREATE INDEX idx_item_ordinal ON item(ordinal);
