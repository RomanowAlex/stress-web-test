package ru.romanow.simple.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.romanow.simple.web.domain.DTZ;
import ru.romanow.simple.web.repository.DTZRepository;
import ru.romanow.simple.web.service.DTZService;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.joining;
import static org.joda.time.DateTime.now;
import static org.joda.time.DateTimeZone.forID;

@SpringBootApplication
public class SimpleWebApplication
        implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(SimpleWebApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SimpleWebApplication.class, args);
    }

    @Autowired
    private DTZService dtzService;

    @Override
    public void run(ApplicationArguments args) {
        final DTZ utc = new DTZ().setDate(now(forID("UTC")));
        logger.info("{}", utc);

        final DTZ now = new DTZ().setDate(now());
        logger.info("{}", now);

        dtzService.save(utc, now);

        final List<DTZ> all = dtzService.findAll();
        logger.info("{}", all.stream().map(DTZ::toString).collect(joining(", ")));

    }
}
