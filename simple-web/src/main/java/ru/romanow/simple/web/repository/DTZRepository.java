package ru.romanow.simple.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.romanow.simple.web.domain.DTZ;

public interface DTZRepository
        extends JpaRepository<DTZ, Integer> {}
