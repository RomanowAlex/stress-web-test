package ru.romanow.simple.web.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import ru.romanow.simple.web.domain.Item;
import ru.romanow.simple.web.model.ItemRequest;
import ru.romanow.simple.web.model.ItemResponse;
import ru.romanow.simple.web.repository.ItemRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.springframework.util.DigestUtils.md5DigestAsHex;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl
        implements ItemService {
    private final ItemRepository itemRepository;
    private static final int BORDER = 10_000_000;

    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<ItemResponse> findItems() {
        return itemRepository
                .findAll(BORDER / 4)
                .stream()
                .map(ItemResponse::ok)
                .collect(toList());
    }

    @Nonnull
    @Override
    @Transactional
    public ItemResponse updateItem(@Nullable Integer id, @Nonnull ItemRequest request) {
        Item item;
        if (id != null) {
            item = itemRepository.findById(id).orElseGet(this::createItem);
        } else {
            item = createItem();
        }

        ofNullable(request.getModel()).ifPresent(item::setModel);
        ofNullable(request.getSize()).ifPresent(item::setSize);
        return ItemResponse.ok(itemRepository.save(item));
    }

    private Item createItem() {
        return new Item()
                .setCode(randomAlphabetic(4))
                .setOrdinal(nextInt(1, BORDER));
    }
}
