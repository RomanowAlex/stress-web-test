package ru.romanow.simple.web.service;

import ru.romanow.simple.web.domain.DTZ;

import javax.annotation.Nonnull;
import java.util.List;

public interface DTZService {

    void save(@Nonnull DTZ ... arr);

    List<DTZ> findAll();
}
