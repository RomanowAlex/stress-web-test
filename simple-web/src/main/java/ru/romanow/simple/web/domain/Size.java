package ru.romanow.simple.web.domain;

public enum Size {
    S, M, L, XL
}
