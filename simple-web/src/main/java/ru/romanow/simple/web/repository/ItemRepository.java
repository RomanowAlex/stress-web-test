package ru.romanow.simple.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.romanow.simple.web.domain.Item;

import java.util.List;

public interface ItemRepository
        extends JpaRepository<Item, Integer> {

    @Query("select i from Item i where i.ordinal > :border")
    List<Item> findAll(@Param("border") Integer border);
}
