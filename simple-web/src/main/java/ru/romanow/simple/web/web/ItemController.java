package ru.romanow.simple.web.web;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.romanow.simple.web.model.ItemRequest;
import ru.romanow.simple.web.model.ItemResponse;
import ru.romanow.simple.web.service.ItemService;

import java.util.List;

@RestController
@RequestMapping("/items")
@RequiredArgsConstructor
public class ItemController {
    private final ItemService itemService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ItemResponse> items() {
        return itemService.findItems();
    }

    @PostMapping(value = {"/", "/{id}"},
                 consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
                 produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ItemResponse update(@PathVariable(required = false) Integer id, @RequestBody ItemRequest request) {
        return itemService.updateItem(id, request);
    }
}