package ru.romanow.simple.web.model;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.romanow.simple.web.domain.Item;
import ru.romanow.simple.web.domain.Size;

import javax.annotation.Nonnull;

@Data
@Accessors(chain = true)
public class ItemResponse {
    private final Result result;
    private Integer id;
    private String model;
    private Size size;
    private Integer ordinal;
    private String code;

    private ItemResponse(@Nonnull Result result) {
        this.result = result;
    }

    public static ItemResponse ok(@Nonnull Item item) {
        final ItemResponse response = new ItemResponse(Result.OK);
        response.id = item.getId();
        response.model = item.getModel();
        response.size = item.getSize();
        response.ordinal = item.getOrdinal();
        response.code = item.getCode();
        return response;
    }

    public static ItemResponse fail() {
        return new ItemResponse(Result.ERROR);
    }
}
