package ru.romanow.simple.web.model;

import lombok.Data;
import ru.romanow.simple.web.domain.Size;

@Data
public class ItemRequest {
    private String model;
    private Size size;
}
