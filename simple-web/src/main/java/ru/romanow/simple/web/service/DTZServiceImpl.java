package ru.romanow.simple.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.simple.web.domain.DTZ;
import ru.romanow.simple.web.repository.DTZRepository;

import javax.annotation.Nonnull;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
@RequiredArgsConstructor
public class DTZServiceImpl
        implements DTZService {
    private final DTZRepository repository;

    @Override
    @Transactional
    public void save(@Nonnull DTZ... arr) {
        repository.saveAll(newArrayList(arr));
    }

    @Override
    @Transactional(readOnly = true)
    public List<DTZ> findAll() {
        return repository.findAll();
    }
}
