package ru.romanow.simple.web.domain;

import com.google.common.base.MoreObjects;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "dtz")
public class DTZ {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTimeAndZoneWithOffset")
    @Columns(columns = {
            @Column(name = "date"),
            @Column(name = "timezone")
    })
    private DateTime date;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("date", date)
                          .toString();
    }
}
