package ru.romanow.simple.web.service;

import ru.romanow.simple.web.model.ItemRequest;
import ru.romanow.simple.web.model.ItemResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface ItemService {

    @Nonnull
    List<ItemResponse> findItems();

    @Nonnull
    ItemResponse updateItem(@Nullable Integer id, @Nonnull ItemRequest request);
}
