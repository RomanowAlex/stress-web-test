package ru.romanow.simple.reactive.model;

import lombok.Data;
import ru.romanow.simple.reactive.domain.Size;

@Data
public class ItemRequest {
    private String model;
    private Size size;
}
