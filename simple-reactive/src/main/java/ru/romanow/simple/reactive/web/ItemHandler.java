package ru.romanow.simple.reactive.web;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ru.romanow.simple.reactive.model.ItemRequest;
import ru.romanow.simple.reactive.model.ItemResponse;
import ru.romanow.simple.reactive.service.ItemService;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Service
@AllArgsConstructor
public class ItemHandler {
    private final ItemService itemService;

    public Mono<ServerResponse> items(ServerRequest request) {
        return ok().body(itemService.findItems(), ItemResponse.class);
    }

    public Mono<ServerResponse> updateItem(ServerRequest serverRequest) {
        Integer id = null;
        if (serverRequest.pathVariables().containsKey("id")) {
            id = Integer.valueOf(serverRequest.pathVariable("id"));
        }
        return ok()
                .body(itemService.updateItem(id, serverRequest.bodyToMono(ItemRequest.class)), ItemResponse.class);
    }
}
