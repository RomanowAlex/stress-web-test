package ru.romanow.simple.reactive.model;

public enum Result {
    OK, ERROR
}
