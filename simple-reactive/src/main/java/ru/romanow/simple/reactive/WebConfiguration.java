package ru.romanow.simple.reactive;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.HandlerFilterFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.romanow.simple.reactive.model.ErrorResponse;
import ru.romanow.simple.reactive.web.ItemHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.status;
import static reactor.core.publisher.Mono.just;


@Configuration
public class WebConfiguration {

    @Bean
    public RouterFunction<ServerResponse> routeRequest(ItemHandler handler) {
        return route()
                .GET("/items", handler::items)
                .POST("/items", handler::updateItem)
                .POST("/items/{id}", handler::updateItem)
                .build();

    }
}
