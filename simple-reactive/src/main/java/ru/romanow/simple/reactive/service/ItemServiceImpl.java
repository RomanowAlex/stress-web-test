package ru.romanow.simple.reactive.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.romanow.simple.reactive.domain.Item;
import ru.romanow.simple.reactive.model.ItemRequest;
import ru.romanow.simple.reactive.model.ItemResponse;
import ru.romanow.simple.reactive.repository.ItemRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static reactor.core.publisher.Mono.just;

@Service
@AllArgsConstructor
public class ItemServiceImpl
        implements ItemService {
    private static final int BORDER = 10_000_000;
    private final ItemRepository itemRepository;

    @Nonnull
    @Override
    public Flux<ItemResponse> findItems() {
        return itemRepository
                .findWithBorder(100)
                .map(ItemResponse::ok);
    }

    @Nonnull
    @Override
    @Transactional
    public Mono<ItemResponse> updateItem(@Nullable Integer id, @Nonnull Mono<ItemRequest> request) {
        Mono<Item> mono = id != null ? itemRepository.findById(id) : createItem();
        return mono
                .switchIfEmpty(createItem())
                .flatMap(item -> request.map(r -> {
                    ofNullable(r.getModel()).ifPresent(item::setModel);
                    ofNullable(r.getSize()).ifPresent(item::setSize);
                    return item;
                }))
                .flatMap(itemRepository::save)
                .map(ItemResponse::ok);
    }

    private Mono<Item> createItem() {
        return just(new Item()
                            .setCode(randomAlphabetic(4))
                            .setOrdinal(nextInt(1, BORDER)));
    }
}
