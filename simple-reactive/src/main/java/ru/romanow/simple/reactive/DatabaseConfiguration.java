package ru.romanow.simple.reactive;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories
public class DatabaseConfiguration
        extends AbstractR2dbcConfiguration {

    @Value("${database.host}")
    private String host;

    @Value("${database.port}")
    private Integer port;

    @Value("${database.db}")
    private String database;

    @Value("${database.user}")
    private String user;

    @Value("${database.password}")
    private String password;

    @Bean
    @Override
    public ConnectionFactory connectionFactory() {
        final PostgresqlConnectionConfiguration build =
                PostgresqlConnectionConfiguration
                        .builder()
                        .host(host)
                        .port(port)
                        .database(database)
                        .username(user)
                        .password(password)
                        .build();

        return new PostgresqlConnectionFactory(build);
    }
}
