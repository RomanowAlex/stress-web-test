package ru.romanow.simple.reactive.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Accessors(chain = true)
public class Item {

    @Id
    private Integer id;
    private String model;
    private Size size;
    private Integer ordinal;
    private String code;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return new EqualsBuilder()
                .append(model, item.model)
                .append(size, item.size)
                .append(ordinal, item.ordinal)
                .append(code, item.code)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(model)
                .append(size)
                .append(ordinal)
                .append(code)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("model", model)
                .append("size", size)
                .append("ordinal", ordinal)
                .append("code", code)
                .toString();
    }
}