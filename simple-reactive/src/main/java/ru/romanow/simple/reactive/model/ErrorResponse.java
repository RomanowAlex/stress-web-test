package ru.romanow.simple.reactive.model;

import lombok.Data;

@Data
public class ErrorResponse {
    private final String message;
}
