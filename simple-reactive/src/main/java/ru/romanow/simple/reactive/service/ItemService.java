package ru.romanow.simple.reactive.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.romanow.simple.reactive.model.ItemRequest;
import ru.romanow.simple.reactive.model.ItemResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface ItemService {

    @Nonnull
    Flux<ItemResponse> findItems();

    @Nonnull
    Mono<ItemResponse> updateItem(@Nullable Integer id, @Nonnull Mono<ItemRequest> request);
}
