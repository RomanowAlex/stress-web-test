package ru.romanow.simple.reactive.repository;

import org.springframework.data.r2dbc.repository.query.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import ru.romanow.simple.reactive.domain.Item;

public interface ItemRepository
        extends ReactiveCrudRepository<Item, Integer> {

    @Query("SELECT i.id, i.model, i.size, i.ordinal, i.code FROM item i WHERE i.ordinal > $1")
    Flux<Item> findWithBorder(Integer border);
}
