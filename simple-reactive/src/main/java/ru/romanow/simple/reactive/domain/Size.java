package ru.romanow.simple.reactive.domain;

public enum Size {
    S, M, L, XL
}
